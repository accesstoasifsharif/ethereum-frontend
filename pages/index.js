import Head from "next/head";
import ConnectWallet from "../components/ConnectWallet";
import SendCoin from "../components/SendCoin";
import styles from "../styles/Home.module.css";
import { useWeb3React } from "@web3-react/core";

export default function Home() {
	const web3reactContext = useWeb3React();

	return (
		<div className={styles.container}>
			<Head>
				<title>Airdrop</title>
				<meta name="Airdrop" content="Get airdrop" />
				<link rel="icon" href="/1487521.png" />
			</Head>

			<main className={styles.main}>
				{web3reactContext.active ? (
					<SendCoin></SendCoin>
				) : (
					<ConnectWallet></ConnectWallet>
				)}
			</main>
		</div>
	);
}
