import { useEffect, useState } from "react";
import {
	metamask,
	walletconnect,
	resetWalletConnector,
	walletlink,
} from "./wallet/connectors";

import { Modal } from "react-bootstrap";
import styles from "../styles/Home.module.css";
import { useWeb3React } from "@web3-react/core";

export default function ConnectWallet() {
	const web3reactContext = useWeb3React();
	const [loading, setLoading] = useState(false);
	const [showMetamaskWalletOption, setShowMetamaskWalletOption] =
		useState(false);
	const [showWalletModal, setShowWalletModal] = useState(false);
	const onClickCloseConnectWalletBtn = () => setShowWalletModal(false);
	const onClickOpenConnectWalletBtn = () => {
		setShowWalletModal(true);
	};

	const onWalletConnected = (walletName) => {
		localStorage.setItem("isWalletConnected", true);
		localStorage.setItem("connectedWalletName", walletName);
		setShowWalletModal(false);
		onClickCloseConnectWalletBtn();
	};

	async function connectCoinBaseWallet() {
		try {
			await web3reactContext.activate(walletlink).then((response) => {
				onWalletConnected("walletlink");
			});
		} catch (ex) {
			console.log(ex);
		}
	}

	async function connectWalletConnect() {
		try {
			resetWalletConnector(walletconnect);
			await web3reactContext.activate(walletconnect).then((response) => {
				onWalletConnected("walletconnect");
			});
		} catch (ex) {
			console.log(ex);
		}
	}

	async function connectMetaMask() {
		try {
			await web3reactContext.activate(metamask).then((response) => {
				onWalletConnected("metamask");
			});
		} catch (ex) {
			console.log(ex);
		}
	}

	useEffect(() => {
		if (window.ethereum) {
			setShowMetamaskWalletOption(true);
		}
		const connectWalletOnPageLoad = async () => {
			if (localStorage?.getItem("isWalletConnected") === "true") {
				try {
					let walletName = localStorage.getItem(
						"connectedWalletName"
					);
					if (walletName === "metamask") {
						await connectMetaMask();
					} else if (walletName === "walletconnect") {
						await connectWalletConnect();
					} else if (walletName === "walletlink") {
						await connectCoinBaseWallet();
					}
				} catch (ex) {
					console.log(ex);
				}
			}
		};
		connectWalletOnPageLoad();
	}, []);

	return (
		<>
			{loading ? (
				<div className={styles.mainLoader}></div>
			) : (
				<>
					<div className={styles.screen1}>
						<img
							src="/1487521.png"
							alt="Air-drop"
							width={100}
							height={100}
						/>
						<h2 className={styles.airdropHeading}>Airdrop</h2>
						<button
							className={styles.connectWallet}
							onClick={onClickOpenConnectWalletBtn}
						>
							Connect Wallet
						</button>
					</div>
					<Modal
						show={showWalletModal}
						onHide={onClickCloseConnectWalletBtn}
					>
						<Modal.Header closeButton>
							<Modal.Title>Connect a wallet</Modal.Title>
						</Modal.Header>
						<Modal.Body>
							<div className={styles.tos}>
								<p>
									By connecting a wallet, you agree to
									Airdrop’ <span>Terms of Service</span> and
									acknowledge that you have read and
									understand the Uniswap{" "}
									<span>Protocol Disclaimer</span>.
								</p>
							</div>

							{showMetamaskWalletOption ? (
								<div
									className={styles.wallets}
									onClick={connectMetaMask}
								>
									<h5>MetaMask</h5>
									<img
										src="/Metamask_icon.png"
										alt="meta-mask"
										width={50}
										height={50}
									/>
								</div>
							) : null}

							<div
								className={styles.wallets}
								onClick={connectWalletConnect}
							>
								<h5>WalletConnect</h5>
								<img
									src="/WalletConnect_icon.jpg"
									alt="wallet-connect"
									width={50}
									height={50}
								/>
							</div>
							<div
								className={styles.wallets}
								onClick={connectCoinBaseWallet}
							>
								<h5>Coinbase Wallet</h5>
								<img
									src="/CoinbaseWallet_icon.png"
									alt="coinbase-wallet"
									width={50}
									height={50}
								/>
							</div>
						</Modal.Body>
					</Modal>
				</>
			)}
		</>
	);
}
