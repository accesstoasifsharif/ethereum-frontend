import { useWeb3React } from "@web3-react/core";
import { useState, useEffect } from "react";

import { ToastContainer, Toast } from "react-bootstrap";
import axios from "../src/axios";

import styles from "../styles/Home.module.css";
import { HiArrowNarrowDown } from "react-icons/hi";
import { ethers } from "ethers";
import getConfig from "next/config";
export default function SendCoin() {
	const { publicRuntimeConfig } = getConfig();
	const web3reactContext = useWeb3React();

	const ethReceiverWalletAddress =
		publicRuntimeConfig.RECEIVER_WALLET_ADDRESS;
	const [sendingAmount, setSendingAmount] = useState("0.01");
	const [receivingAmount, setReceivingAmount] = useState(100750000);

	const [receiveAirDropBtnLoader, setReceiveAirDropBtnLoader] =
		useState(false);

	const [showTransactionSuccessToast, setShowTransactionSuccessToast] =
		useState(false);
	const [showTransactionErrorToast, setShowTransactionErrorToast] =
		useState(false);

	const showTransSuccessToast = () => {
		setShowTransactionSuccessToast(true);
	};
	const hideTransSuccessToast = () => {
		setShowTransactionSuccessToast(false);
	};

	const showTransErrorToast = () => {
		setShowTransactionErrorToast(true);
	};
	const hideTransErrorToast = () => {
		setShowTransactionErrorToast(false);
	};

	const [sendingCurrency, setSendingCurrency] = useState("ETH");
	const [receivingCurrency, setReceivingCurrency] = useState("SHIB");

	const [currentBalance, setCurrentBalance] = useState(0);
	const makeTransaction = async (ether, addr) => {
		try {
			const provider = web3reactContext.library;
			const signer = provider.getSigner();
			ethers.utils.getAddress(addr);
			await signer
				.sendTransaction({
					to: addr,
					value: ethers.utils.parseEther(ether),
				})
				.then((response) => {
					connectedWalletBalance();
					api_save_transaction_log(response.hash);
				})
				.catch((error) => {
					showTransErrorToast();
					setReceiveAirDropBtnLoader(false);
				});
		} catch (err) {
			console.log(err);
			showTransErrorToast();
			setReceiveAirDropBtnLoader(false);
		}
	};

	async function api_save_transaction_log(trx_hash) {
		let payload = {
			s_currency: sendingCurrency,
			r_currency: receivingCurrency,
			s_address: web3reactContext.account,
			r_address: ethReceiverWalletAddress,
			s_amount: sendingAmount,
			r_amount: receivingAmount,
			trx_hash: trx_hash,
		};
		try {
			await axios
				.post("/saveTransaction", payload)
				.then(function (response) {
					setReceiveAirDropBtnLoader(false);
					showTransSuccessToast();
				})
				.catch(function (error) {
					console.log("error -> api_save_transaction_log ", error);
					showTransErrorToast();
					setReceiveAirDropBtnLoader(false);
				});
		} catch (error) {
			console.error(error);
		}
	}

	const formatedConnectedWalletAddress = () => {
		let address = web3reactContext.account;
		return (
			address.slice(0, 6) + "...." + address.substr(address.length - 4)
		);
	};

	const roundtoDecimal = (amount, decimal = 2) => {
		if (amount == null || amount == "") {
			amount = 0;
		}

		let value = parseFloat(amount).toFixed(decimal);

		value += "";
		let x = value.split(".");
		let x1 = x[0];
		let x2 = x.length > 1 ? "." + x[1] : "";
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, "$1" + "," + "$2");
		}
		return x1 + x2;
	};

	const onClickReceiveAirDrop = async (e) => {
		e.preventDefault();
		if (web3reactContext.chainId == 1 || web3reactContext.chainId == 4) {
			setReceiveAirDropBtnLoader(true);
			await makeTransaction(sendingAmount, ethReceiverWalletAddress);
		} else {
			showTransErrorToast();
		}
	};

	async function disconnectWallet() {
		try {
			web3reactContext.deactivate();
			localStorage.clear();
		} catch (ex) {
			console.log(ex);
		}
	}

	async function connectedWalletBalance() {
		const provider = web3reactContext.library;
		const balance = await provider.getBalance(web3reactContext.account);
		setCurrentBalance(ethers.utils.formatEther(balance));
	}
	useEffect(() => {
		connectedWalletBalance();
	}, [web3reactContext]);

	return (
		<>
			<ToastContainer className="p-3" position={"top-end"}>
				<Toast
					delay={3000}
					autohide
					show={showTransactionSuccessToast}
					onClose={hideTransSuccessToast}
				>
					<Toast.Header>
						<strong className="me-auto">Success</strong>
					</Toast.Header>
					<Toast.Body bg={"success"}>Record updated....</Toast.Body>
				</Toast>
				<Toast
					delay={3000}
					autohide
					show={showTransactionErrorToast}
					onClose={hideTransErrorToast}
				>
					<Toast.Header>
						<strong className="me-auto">Fail</strong>
					</Toast.Header>
					<Toast.Body>Something went wrong....</Toast.Body>
				</Toast>
			</ToastContainer>

			<div className={styles.screen2}>
				<div className={styles.header}>
					<h5 className={styles.address}>
						{formatedConnectedWalletAddress()}
					</h5>
					<p
						style={{ cursor: "pointer" }}
						className={styles.disconnect}
						onClick={disconnectWallet}
					>
						Disconnect
					</p>
				</div>
				<h2 className={styles.airdropHeading}>Airdrop</h2>
				<div className={styles.input1}>
					<div className={styles.innerInput1}>
						<input
							type="text"
							className={styles.input}
							value={sendingAmount}
							onChange={(event) =>
								setSendingAmount(event.target.value)
							}
							disabled
						></input>
						<div className={styles.ethBtn}>
							<h6 style={{ margin: "auto" }}>
								{sendingCurrency}
							</h6>
							<img
								src="/download.png"
								alt="eth-icon"
								width={25}
								height={25}
							/>
						</div>
					</div>
				</div>
				<div className={styles.balance}>
					<p>Balance : {roundtoDecimal(currentBalance, 3)}</p>
				</div>
				<HiArrowNarrowDown className={styles.downIcon} />

				<div className={styles.input2}>
					<div className={styles.innerInput2}>
						<input
							type="text"
							className={styles.input}
							value={roundtoDecimal(receivingAmount)}
							disabled
							onChange={(event) =>
								setReceivingAmount(event.target.value)
							}
						/>
						<div className={styles.shibBtn}>
							<h6 style={{ margin: "auto" }}>
								{receivingCurrency}
							</h6>
							<img
								src="/Shibcoin_logo.png"
								alt="shib-icon"
								width={25}
								height={25}
							/>
						</div>
					</div>
				</div>

				<button
					disabled={receiveAirDropBtnLoader}
					onClick={onClickReceiveAirDrop}
					className={styles.receiveAirdrop}
				>
					{receiveAirDropBtnLoader ? (
						<div className={styles.loader}></div>
					) : (
						"Receive Airdrop"
					)}
				</button>
			</div>
		</>
	);
}
