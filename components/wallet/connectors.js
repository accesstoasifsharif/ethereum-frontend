import { InjectedConnector } from "@web3-react/injected-connector";
import { WalletConnectConnector } from "@web3-react/walletconnect-connector";
import { WalletLinkConnector } from "@web3-react/walletlink-connector";

const RPC_URLS = {
	1: "https://mainnet.infura.io/v3/1f479aba5af745f9929b6ce6b75f3711",
	4: "https://rinkeby.infura.io/v3/1f479aba5af745f9929b6ce6b75f3711",
};

//metamask
export const metamask = new InjectedConnector({
	supportedChainIds: [1, 3, 4, 5, 42],
});

// walletconnect
export const walletconnect = new WalletConnectConnector({
	rpc: {
		1: RPC_URLS[1],
		4: RPC_URLS[4],
	},
	qrcode: true,
	pollingInterval: 15000,
});

export function resetWalletConnector(connector) {
	if (connector && connector instanceof WalletConnectConnector) {
		connector.walletConnectProvider = undefined;
	}
}

//coinbase wallet
export const walletlink = new WalletLinkConnector({
	url: RPC_URLS[4],
	appName: "Ethereum DApp",
	supportedChainIds: [1, 4],
});
