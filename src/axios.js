import axios from "axios";
import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();
export default axios.create({
	baseURL: publicRuntimeConfig.API_BASE_URL + "/api",
	headers: {
		"Content-Type": "application/json",
	},
});
