/** @type {import('next').NextConfig} */
const nextConfig = {
	reactStrictMode: true,
	publicRuntimeConfig: {
		RECEIVER_WALLET_ADDRESS: process.env.RECEIVER_WALLET_ADDRESS,
		API_BASE_URL: process.env.API_BASE_URL,
	},
};

module.exports = nextConfig;
